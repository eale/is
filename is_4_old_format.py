import math
import os
from collections import Counter, defaultdict


# Функция Term frequency, как часто слова встречаются в каждом доке
def calculate_tf(document):
    words = document.split()
    word_counts = Counter(words)
    total_words = len(words)
    tf_dict = {word: round(count / total_words, 6) for word, count in word_counts.items()}

    return tf_dict


# Функция подсчета Inverse Document Frequence (IDF), коэфицент указывающий насколько слово уникально для всех документов
def calculate_idf(documents):
    idf_text = defaultdict(lambda: 0)
    total_docs = len(documents)
    for text in documents:
        for word in set(text.split()):
            idf_text[word] += 1
    return {word: round(math.log10(total_docs / count), 6) for word, count in idf_text.items()}


# Функция подсчета TF-IDF насколько важно слово для конкретного документа в сравнении с другими документами.
def calculate_tf_idf(tf, idf):
    tf_idf_text = {}
    for doc, tf_vals in tf.items():
        tf_idf_text[doc] = {word: round(tf_val * idf[word], 6) for word, tf_val in tf_vals.items()}
    return tf_idf_text


def main():
    documents = []
    input_directory = 'is_2'
    output_directory = 'is_4'

    for filename in os.listdir(input_directory):
        if filename.endswith(".txt"):
            with open(os.path.join(input_directory, filename), 'r', encoding='utf-8') as file:
                line = file.read()
                documents.append(line)

    tf_result = {f"doc_{i}": calculate_tf(doc) for i, doc in enumerate(documents)}
    idf_result = calculate_idf(documents)
    tf_idf_result = calculate_tf_idf(tf_result, idf_result)

    print(tf_result)

    # pd.DataFrame(tf_result.items(), columns=['Word', 'TF']).to_csv(f'{output_directory}/tf.csv', index=False)
    # pd.DataFrame(idf_result.items(), columns=['Word', 'IDF']).to_csv(f'{output_directory}/idf.csv', index=False)
    # pd.DataFrame(tf_idf_result.items(), columns=['Word', 'TF-IDF']).to_csv(f'{output_directory}/tf_idf.csv', index=False)


if __name__ == '__main__':
    main()
