import csv
from collections import defaultdict
from math import sqrt


def convert_query_to_tfidf_vector(query: str, idf_values: dict):
    words = query.split()
    vector = defaultdict(float)
    for word in words:
        vector[word] += 1.0
    for word, freq in vector.items():
        vector[word] = freq * idf_values.get(word, 0.0)  # Apply IDF values
    return vector


def calculate_cosine_distance(vector_one: dict, vector_two: dict):
    dot_product = sum(
        vector_one[word] * vector_two.get(word, 0.0) for word in vector_one)
    norm_vector_one = sqrt(sum(freq ** 2 for freq in vector_one.values()))
    norm_vector_two = sqrt(sum(freq ** 2 for freq in vector_two.values()))
    return round(dot_product / (norm_vector_one * norm_vector_two), 6) if norm_vector_one * norm_vector_two != 0 else 0


def search(query, documents_tfidf_values, idf):
    query_vector = convert_query_to_tfidf_vector(query, idf)
    results = [(doc_id, calculate_cosine_distance(query_vector, doc_vector)) for doc_id, doc_vector in
               documents_tfidf_values.items()]
    results.sort(key=lambda x: x[1], reverse=True)
    return results


def fetch_data_from_files(input_directory: str):
    documents_tfidf = {}
    idf = {}
    with open(f"{input_directory}/tf_idf.csv", 'r', encoding='utf-8') as file:
        reader = csv.reader(file)
        next(reader, None)  # Skip the header
        for row in reader:
            document_id, tfidf_values = row
            documents_tfidf[document_id] = eval(tfidf_values)

    with open(f"{input_directory}/idf.csv", 'r', encoding='utf-8') as file:
        reader = csv.reader(file)
        next(reader, None)  # Skip the header
        for row in reader:
            term, value = row
            idf[term] = float(value)

    return documents_tfidf, idf


def main():
    input_directory = "is_4"
    output_directory = "is_5"

    documents_tfidf, idf = fetch_data_from_files(input_directory)
    queries = ['облако', "облако солнце", "облако солнце тинькофф"]
    with open(f'{output_directory}/result.txt', 'w+', encoding='utf-8') as file:
        for query in queries:
            search_results = search(query, documents_tfidf, idf)
            res = [query]
            for doc_id, similarity in search_results:
                if similarity > 0.001:
                    res.append(f"Document: {doc_id}, Similarity: {similarity}")
            file.write("\n".join(res) + "\n\n")


if __name__ == '__main__':
    main()
