import os
from collections import defaultdict

from pymorphy2 import MorphAnalyzer

morph_analyzer = MorphAnalyzer()


def split_into_tokens(text):
    return [word.lower() for word in text.split()]


def get_inverted_index(docs):
    index = defaultdict(list)
    for doc_id, content in docs.items():
        for token in set(split_into_tokens(content)):
            index[token].append(doc_id.replace(".txt", ""))
    return index


def write_inverted_index(index, path):
    with open(path, 'w+', encoding='utf-8') as file:
        for term, ids in sorted(index.items()):
            file.write(f"{term}: {' '.join(map(str, ids))}\n")


def read_inverted_index(path):
    index = {}
    with open(path, 'r', encoding='utf-8') as file:
        for line in file:
            term, doc_ids_str = line.strip().split(': ')
            index[term] = list(map(int, doc_ids_str.split()))
    return index


def boolean_search(expression, index, total_docs):
    expression_tokens = expression.split()
    all_docs = set(range(1, total_docs))

    result = set()
    current_op = '|'

    for token in expression_tokens:
        if token in ["&", "|"]:
            current_op = token
            continue

        if token.startswith("!"):
            term = token[1:]
            current_set = all_docs - set(index.get(term, []))
        else:
            current_set = set(index.get(token, []))

        if not result:
            result = current_set
        elif current_op == "&":
            result &= current_set
        elif current_op == "|":
            result |= current_set

    return result



def main():
    input_directory = 'is_2'
    output_directory = 'is_3'
    inverted_index_path = os.path.join(output_directory, 'inverted_index.txt')

    docs = {}
    for filename in os.listdir(input_directory):
        if filename.endswith(".txt"):
            with open(os.path.join(input_directory, filename), 'r', encoding='utf-8') as file:
                docs[filename] = file.read()

    index = get_inverted_index(docs)
    write_inverted_index(index, inverted_index_path)

    # Загрузка индекса и выполнение поисковых запросов
    index = read_inverted_index(inverted_index_path)
    total_docs = len(docs)
    queries = [
        "облако & солнце | тинькофф",
        "облако & !солнце | !тинькофф",
        "облако | солнце | тинькофф",
        "облако | !солнце | !тинькофф",
        "облако & солнце & тинькофф"
    ]

    search_result_path = os.path.join(output_directory, 'search_results.txt')

    with open(search_result_path, 'w+', encoding='utf-8') as file:
        for query in queries:
            result = boolean_search(query, index, total_docs)
            result = None if not result else result
            file.write(f"{query}: {result}\n")


if __name__ == '__main__':
    main()
