import math
import os
from collections import Counter, defaultdict

import pandas as pd


# Функция Term frequency, как часто слова встречаются в каждом доке
def calculate_tf(document):
    words = document.split()
    word_counts = Counter(words)
    total_words = len(words)
    tf_dict = {word: round(count / total_words, 6) for word, count in word_counts.items()}
    return tf_dict


# Функция подсчета Inverse Document Frequence (IDF), коэфицент указывающий насколько слово уникально для всех документов
def calculate_idf(documents):
    idf_text = defaultdict(lambda: 0)
    total_docs = len(documents)
    for text in documents:
        for word in set(text.split()):
            idf_text[word] += 1
    return {word: round(math.log10(total_docs / count), 6) for word, count in idf_text.items()}


# Функция подсчета TF-IDF насколько важно слово для конкретного документа в сравнении с другими документами.
def calculate_tf_idf(tf, idf):
    tf_idf_text = {}
    for doc, tf_vals in tf.items():
        tf_idf_text[doc] = {word: round(tf_val * idf[word], 6) for word, tf_val in tf_vals.items()}
    return tf_idf_text


def main():
    documents = []
    input_directory = 'is_2'
    output_directory = 'is_4'

    os.makedirs(output_directory, exist_ok=True)

    for filename in os.listdir(input_directory):
        if filename.endswith(".txt"):
            with open(os.path.join(input_directory, filename), 'r', encoding='utf-8') as file:
                documents.append(file.read())

    tf_result = {f"doc_{i}": calculate_tf(doc) for i, doc in enumerate(documents)}
    idf_result = calculate_idf(documents)
    tf_idf_result = calculate_tf_idf(tf_result, idf_result)

    words = set(word for doc in documents for word in doc.split())
    tf_data = {word: [0] * len(documents) for word in words}
    for i, (doc_name, word_dict) in enumerate(tf_result.items()):
        for word, value in word_dict.items():
            tf_data[word][i] = value

    idf_data = {"Word": list(idf_result.keys()), "IDF": list(idf_result.values())}

    pd.DataFrame(tf_data, index=[f"doc_{i}" for i in range(len(documents))]).transpose().to_csv(
        os.path.join(output_directory, 'tf.csv'))
    pd.DataFrame(idf_data).to_csv(os.path.join(output_directory, 'idf.csv'))

    df_tf_idf = pd.DataFrame({word: [0] * len(documents) for word in words},
                             index=[f"doc_{i}" for i in range(len(documents))]).transpose()
    for doc_name, word_dict in tf_idf_result.items():
        for word, value in word_dict.items():
            df_tf_idf.loc[word, doc_name] = value
    df_tf_idf.to_csv(os.path.join(output_directory, 'tf_idf.csv'))


if __name__ == '__main__':
    main()
