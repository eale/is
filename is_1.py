import re

import requests
from bs4 import BeautifulSoup


def is_valid_url(url):
    return bool(re.match(r'https?://', url))


def download_pages(start_urls, base_path="web_crawler/is_1", max_pages=100, min_words=1000):
    visited_urls = set()
    pages_to_visit = {start_urls.pop()}
    page_counter = 0

    with open(f'{base_path}/index.txt', 'w+', encoding='utf-8') as index_file:
        index_file.write('')

    while pages_to_visit or start_urls and page_counter < max_pages:
        current_url = pages_to_visit.pop()
        if current_url in visited_urls or not is_valid_url(current_url):
            continue

        try:
            response = requests.get(current_url, timeout=10)
            soup = BeautifulSoup(response.content, 'html.parser')
            text = soup.get_text(separator=' ', strip=True)
            words = text.split()

            if len(words) >= min_words:
                filename = f'{base_path}/{page_counter + 1}.txt'
                with open(filename, 'w', encoding='utf-8') as f:
                    f.write(text)

                with open(f'{base_path}/index.txt', 'a', encoding='utf-8') as index_file:
                    index_file.write(f'{page_counter + 1}: {current_url}\n')

                page_counter += 1
                print(f'Downloaded: {current_url} as {filename}')

            for link in soup.find_all('a', href=True):
                href = link.get('href')
                if is_valid_url(href) and href not in visited_urls:
                    pages_to_visit.add(href)

        except Exception as e:
            print(f'Error downloading {current_url}: {e}')

        visited_urls.add(current_url)

        if not pages_to_visit and start_urls:
            pages_to_visit = {start_urls.pop()}

    print('Download completed.')


def main():
    start_urls = [
        'https://eda.yandex.ru/moscow?shippingType=delivery',
        'https://www.tinkoff.ru/',
        'https://www.kinopoisk.ru/film/807682/?ysclid=lshgfhtn6f650555871',
    ]
    download_pages(start_urls)


if __name__ == '__main__':
    main()
