import os
import re

import pymorphy2
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

nltk.download('stopwords')
nltk.download('punkt')
russian_stopwords = stopwords.words('russian')

cyrillic_pattern = re.compile('^[а-яА-ЯёЁ]+$')

morph = pymorphy2.MorphAnalyzer()


def process_document(file_path, output_directory):
    with open(file_path, 'r', encoding='utf-8') as file:
        text = file.read()

    # Токенизация
    tokens = word_tokenize(text, language="russian")

    # Лемматизация(привести к базовой форме) и фильтрация стоп-слов, оставляем только русские
    lemmas = [morph.parse(token)[0].normal_form for token in tokens if
              token.lower() not in russian_stopwords and token.isalpha() and cyrillic_pattern.match(token)]

    output_file_path = os.path.join(output_directory, os.path.basename(file_path))
    with open(output_file_path, 'w+', encoding='utf-8') as output_file:
        output_file.write(' '.join(lemmas))


def process_all_documents(input_directory, output_directory):
    os.makedirs(output_directory, exist_ok=True)

    for document in os.listdir(input_directory):
        file_path = os.path.join(input_directory, document)
        process_document(file_path, output_directory)
        print(f"Processed and saved: {document}")


def main():
    input_documents_directory = 'is_1'
    output_documents_directory = 'is_2'
    process_all_documents(input_documents_directory, output_documents_directory)


if __name__ == '__main__':
    main()
